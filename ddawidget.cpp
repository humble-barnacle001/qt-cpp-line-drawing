#include "ddawidget.h"
#include "ui_dda.h"

#include <QPaintEvent>
#include <QPainter>

#include <iostream>

DDAWidget::DDAWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Form)
{

    std::cout << "Hello" << std::endl;
    m_true=false;

    ui->setupUi(this);
}

DDAWidget::~DDAWidget()
{
    delete ui;
}

void DDAWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    dda(&painter);
}

void DDAWidget::dda(QPainter *qp)
{
    std::cout << "UPDATE" << std::endl;
    m_true=!m_true;

    float x1=100,y1=100,x2=200,y2=150,dx,dy,len;
    int i;
    QPen pen(m_true? Qt::darkCyan : Qt::darkRed ,5,Qt::SolidLine);
    qp->setPen(pen);
    float x,y,xinc,yinc;
    dx=(x2-x1);
    dy=(y2-y2);
    if(abs(dx)>=abs(dy))
        len=dx;
    else
        len=dy;
    xinc=float(dx)/len;
    yinc=float(dy/len);
    x=x1;
    y=y1;
    qp->drawPoint(x,y);
    for(i=1;i<=len;i++)
    {
        x=x+xinc;
        y=y+yinc;
        qp->drawPoint(x,y);
    }
}

void DDAWidget::on_data_passed(const char* s)
{
    std::cout << s << std::endl ;
}

