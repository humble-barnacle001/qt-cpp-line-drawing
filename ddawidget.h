#ifndef DDAWIDGET_H
#define DDAWIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Form; }
QT_END_NAMESPACE

class DDAWidget : public QWidget
{
    Q_OBJECT

public:
    DDAWidget(QWidget *parent = nullptr);
    ~DDAWidget();

private:
    Ui::Form *ui;
    bool m_true;
    void paintEvent(QPaintEvent *);
    void dda(QPainter *);

public slots:
    void on_data_passed(const char *);

};

#endif // DDAWIDGET_H

