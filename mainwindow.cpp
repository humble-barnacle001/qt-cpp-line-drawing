#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    // Set defaults
    connect(this, &MainWindow::send_textData, ui->widget, &DDAWidget::on_data_passed);

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_closeButton_clicked()
{
    this->close();
}


void MainWindow::on_pushButton_clicked()
{
    // update();
    emit send_textData("Clicked");
}

